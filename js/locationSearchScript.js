function go() {
	var firebaseRef = firebase.database();
		    	var location = [];
        		if (navigator.geolocation) {
            		navigator.geolocation.getCurrentPosition(showPosition);
        		}
        		else
		        {
		            alert("no geolocation :c");
		        }

		        showPosition();

		        function showPosition(position) {
		            location.push(position.coords.latitude);
		            location.push(position.coords.longitude);
		            alert(`Your location: ${location}`);
		            
		            //check if location is near any stores!!
		            var stores = firebaseRef.ref('storeCoords');
		            stores.on('value', checkStores);
		            
		            function checkStores(datasnapshot) {
		                datasnapshot.forEach(function(childDatasnapshot) {
		                    var store = childDatasnapshot.val(); //gives an array of the coords of one store
		                    //values are temporarily 10 so it works but rmb to change back! 0.0005
		                    if ((Math.abs(location[0]-store[0]) < 10) && (Math.abs(location[1]-store[1]) < 10))
		                    {
		                        var storeName = childDatasnapshot.key;
		                        sessionStorage.setItem("sent", storeName); 
		                        window.open('chatbot.html', '_top');
		                    }
		                })
		            }
		        }
			}
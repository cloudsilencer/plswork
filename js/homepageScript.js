$(document).ready(function () {
    var userName = $("#userName");
    var userDetails = $("#userDetails");

    $("#sideBarBtn").click(function () {
        $(".sideBar").toggleClass("active");
    });
    
    $("#logOut").click(function() {
        if (userName.html() !== "Guest")
        {
            alert("Logging out...");
            $(".sideBar").toggleClass("active");
            userName.html("Guest");
            userDetails.html("userDetails");
        }
        else 
        {
            alert("You aren't logged in!")
        }
    });
    
    $("#signIn").click(function() {
        document.getElementById('signInForm').style.display='block';
    });

    $("#signInForm .close, #signInForm .cancelBtn").click(function() {
        document.getElementById('signInForm').style.display='none';
    });

    $("#quedy").click(function() {
        window.open('chatbot.html', '_top');
    });
    
    $("#signUp").click(function() {
        document.getElementById('signUpForm').style.display='block';
    });
    
    $("#signUpForm .close, #signUpForm .cancelBtn").click(function() {
        document.getElementById('signUpForm').style.display='none';
    });

    $("#map").click(function() {
        window.open("locationSearch.html", "_top");
    });
    
    
var firebaseRef = firebase.database();

/****** FOR USER SIGN UP ******/
document.getElementById("signUpBtn").addEventListener("click", function() {
    var SUname = $("input[name='SUname']");
    var SUemail = $("input[name='SUemail']");
    var SUpw = $("input[name='SUpsw']");
    var submit = $("#signUpBtn");
    
    //check if user is existing
    //if yes, reject. if not, pass.
    var userNames = [];
    var users = firebaseRef.ref('users');
    users.on('value', function(datasnapshot) {
        datasnapshot.forEach(function(childDatasnapshot) {
            var user = childDatasnapshot.val();
            userNames.push(user.name);
        })
    });

    checkUser();
    
    function checkUser(datasnapshot) {
        var i = userNames.indexOf(SUname.val());
        if (i > -1)
        {
            alert("This username already exists. Please try a new one.");
            return;
        }
        else 
        {
            //push user data 
            var newUserKey = firebaseRef.ref().child('users').push().key;
            firebaseRef.ref('users/'+newUserKey+'/name').set(SUname.val());
            firebaseRef.ref('users/'+newUserKey+'/email').set(SUemail.val());
            firebaseRef.ref('users/'+newUserKey+'/password').set(SUpw.val());
            
            alert(`Account created successfully. Welcome, ${SUname.val()}!`);
            document.getElementById('signUpForm').style.display='none';
        }
        return;
    }
}, false);
/******************************/

/********* FOR SIGN IN ********/
document.getElementById("signInBtn").addEventListener("click", function() {
    var SIname = $("input[name='SIname']");
    var SIpw = $("input[name='SIpsw']");
    var users = firebaseRef.ref('users');
    var userNames = [];
    var userPw = [];
    var userEmails = [];
    users.on('value', checkUser);

    function checkUser(datasnapshot) {
        datasnapshot.forEach(function(childDatasnapshot) {
            var user = childDatasnapshot.val();
            userNames.push(user.name);
            userPw.push(user.password);
            userEmails.push(user.email);
        })

        //check if user exists
        //login if yes, reject if no
        var i = userNames.indexOf(SIname.val());
        if (i > -1)
        {
            if (userPw[i] == SIpw.val())
            {
                alert("Welcome, " + SIname.val());
                document.getElementById("userName").innerHTML = userNames[i];
                document.getElementById("userDetails").innerHTML = userEmails[i];
                document.getElementById('signInForm').style.display='none';
            }
            else 
            {
                alert("Incorrect password. Please try again.")
            }
        }
        else 
        {
            alert("Incorrect username. Please try again.");
        }
    }
});
/******************************/

    //code to run when the start button is clicked
    document.getElementById("startBtn").addEventListener("click", function() { 
        var location = [];
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        }
        else
        {
            alert("no geolocation :c");
        }

        function showPosition(position) {
            location.push(position.coords.latitude);
            location.push(position.coords.longitude);
            alert(`Your location: ${location}`);
            
            //check if location is near any stores!!
            var stores = firebaseRef.ref('storeCoords');
            stores.on('value', checkStores);
            
            function checkStores(datasnapshot) {
                datasnapshot.forEach(function(childDatasnapshot) {
                    var store = childDatasnapshot.val(); //gives an array of the coords of one store
                    //values are temporarily 10 so it works but rmb to change back! 0.0005
                    if ((Math.abs(location[0]-store[0]) < 10) && (Math.abs(location[1]-store[1]) < 10))
                    {
                        var storeName = childDatasnapshot.key;
                        sessionStorage.setItem("sent", storeName); 
                        window.open('chatbot.html', '_top');
                    }
                })
            }
        }
        

        

        
    });
}); //end of document.ready()